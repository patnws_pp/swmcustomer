@extends('layouts.client')
@section('pageTitle'){{ __('messages.chanenameserver') }}  {{ $domainget->domain }}@parent @endsection
@section('content')

    <div class="row">
        <div class="col-md-6 offset-md-3">

                    <h6 class="m-0 font-weight-bold text-primary">{{ __('messages.chanenameserver') }} {{ $domainget->domain }}</h6>
                    <form  action="{{route('update.dnsgo')}}" method="post">
                        {!! csrf_field() !!}
                        <div class="input-group">
                            <input type="hidden" class="form-control bg-light border-0 small" name="orderid" value="{{ $domainget->orderid }}"  required>
                        </div>
                        <div class="input-group">
                            <label class="col-form-label small"> NS1 : </label>
                            <input type="text" class="form-control bg-light border-0 small" placeholder="ns1 " name="ns1" value="{{ empty($apidata_json['ns1']) ? '' : $apidata_json['ns1'] }}"  required>
                        </div>
                        <div class="input-group">
                            <label class="col-form-label small"> NS2 : </label>
                            <input type="text" class="form-control bg-light border-0 small" placeholder="ns2 " name="ns2" value="{{ empty($apidata_json['ns2']) ? '' : $apidata_json['ns2'] }}" required>
                        </div>
                        <div class="input-group">
                            <label class="col-form-label small"> NS3 : </label>
                            <input type="text" class="form-control bg-light border-0 small" placeholder="ns3 " name="ns3" value="{{ empty($apidata_json['ns3']) ? '' : $apidata_json['ns3'] }}">
                        </div>
                        <div class="input-group">
                            <label class="col-form-label small"> NS4 : </label>
                            <input type="text" class="form-control bg-light border-0 small" placeholder="ns4 " name="ns4" value="{{ empty($apidata_json['ns4']) ? '' : $apidata_json['ns4'] }}">
                        </div>
                        <div class="input-group">
                        <input type="submit" class="btn btn-primary btn-user btn-block my-3" value="{{ __('messages.save') }}">

                        </div>
                    </form>

            </div>
        </div>

@endsection
